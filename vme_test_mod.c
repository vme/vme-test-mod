/*
 *   Copyright (C) 2016 Collabora Ltd.
 *
 *   This program is free software;  you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY;  without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 *   the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program;  if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <linux/module.h>
#include <linux/device.h>
#include <linux/random.h>
#include <linux/slab.h>
#include <linux/vme.h>

/*
 * This module is designed to test various features of the VME bus. Initially
 * read, write, location monitors and interrupts.
 *
 * To achieve this, the following happens at module probe:
 * 1) Allocate 2 master and 2 slave windows.
 * 2) Configure first master over first slave and second master over second
 *    slave.
 * 3) Allocate location monitor, positioning over end of first slave and
 *    attaching callback.
 * 4) Attach interrupt callback.
 * 5) Create buffer of data.
 * 6) Write buffer from first master to first slave. Writing into location
 *    monitor at end of copy.
 * 7) Location monitor callback writes data from slave buffer 1 to slave buffer
 *    2 then raises interrupt.
 * 8) Interrupt callback copies data into another buffer via master 2 and
 *    compares data in recieved buffer to sent buffer.
 *
 * This process can eventually be split over 2 boards so as to test
 * communications between them.
 */

#define WIN_SIZE 0x10000

struct vme_dev *vme_dev;
struct vme_resource *master_res[2];
struct vme_resource *slave_res[2];
struct vme_resource *lm_res;
dma_addr_t slave_dma[2];
void *slave_ptr[2];
void *writebuf;
void *readbuf;

char message[] = "Hello World\n";

static void vme_test_mod_lm_callback(void *data)
{
	char *string = (char *)data;

	printk("vme_test_mod_lm_callback() message: %s\n", string);

	memcpy(slave_ptr[1], slave_ptr[0], WIN_SIZE);

	vme_irq_generate(vme_dev, 1, 1);
}

static void vme_test_mod_int_handler(int level, int statid, void *data)
{
	int retval;
	ssize_t copied;

	printk("vme_test_mod_int_handler()\n");

	copied = vme_master_read(master_res[0], readbuf,
			WIN_SIZE - (8 * sizeof(u32)), 0);
	if (copied != (WIN_SIZE - (8 * sizeof(u32)))) {
		printk("Read failed\n");
		return;
	}

	retval = memcmp(writebuf, readbuf, WIN_SIZE - (8 * sizeof(u32)));
	if (retval != 0)
		printk("FAIL: Buffers don't match!\n");
	else
		printk("PASS: Buffers match\n");
}

static int vme_test_mod_match(struct vme_dev *vdev)
{
	printk("Running vme_test_mod_match()\n");
	return 1;
}

static int vme_test_mod_probe(struct vme_dev *dev)
{
	int i;
	int ret;
	ssize_t copied;

	printk("Running vme_test_mod_probe()\n");

	vme_dev = dev;

	for (i = 0; i < 2; i++) {
		master_res[i] = vme_master_request(dev, VME_A32, VME_SCT,
				VME_D32);
		if (master_res[i] == NULL) {
			printk("Failed to allocate master windows\n");
			ret = -ENODEV;
			goto err_master;
		}
	}

	for (i = 0; i < 2; i++) {
		slave_res[i] = vme_slave_request(dev, VME_A32, VME_SCT);
		if (slave_res[i] == NULL) {
			printk("Failed to allocate slave windows\n");
			ret = -ENODEV;
			goto err_slave;
		}
	}

	lm_res = vme_lm_request(dev);
	if (lm_res == NULL) {
		printk("Failed to allocate location monitor\n");
		ret = -ENODEV;
		goto err_lm;
	}

	ret = vme_lm_count(lm_res);
	printk("Location monitor provides %d consecutive locations\n", ret);

	printk("Configure master windows\n");
	for (i = 0; i < 2; i++) {
		ret = vme_master_set(master_res[i], 1, i * WIN_SIZE, WIN_SIZE,
				VME_A32, VME_SCT, VME_D32);
		if (ret != 0)
			goto err_master_set;
	}

	printk("Allocating slave windows\n");
	for (i = 0; i < 2; i++) {
		slave_ptr[i] = vme_alloc_consistent(slave_res[i], WIN_SIZE,
				&slave_dma[i]);
		if (slave_ptr[i] == NULL) {
			ret = -ENOMEM;
			goto err_alloc;
		}
	}

	printk("Configure slave windows\n");
	for (i = 0; i < 2; i++) {
		ret = vme_slave_set(slave_res[i], 1, i * WIN_SIZE, WIN_SIZE,
				slave_dma[i], VME_A32, VME_SCT);
		if (ret != 0) {
			ret = -EINVAL;
			goto err_slave_set;
		}
	}

	printk("Configure interrupt\n");
	ret = vme_irq_request(dev, 1, 1, vme_test_mod_int_handler, NULL);
	if (ret != 0) {
		ret = -EINVAL;
		goto err_int;
	}

	printk("Configure location monitor\n");
	ret = vme_lm_set(lm_res, WIN_SIZE - (8 * sizeof(u32)), VME_A32,
			VME_SCT);
	if (ret != 0) {
		ret = -EINVAL;
		goto err_lm_set;
	}

	vme_lm_attach(lm_res, 0, vme_test_mod_lm_callback, &message);	
	if (ret != 0) {
		ret = -EINVAL;
		goto err_lm_attach;
	}

	printk("Allocate data buffers\n");
	writebuf = kmalloc(WIN_SIZE, GFP_KERNEL);
	if (writebuf == NULL) {
		ret = -ENOMEM;
		goto err_writebuf;
	}

	readbuf = kmalloc(WIN_SIZE, GFP_KERNEL);
	if (readbuf == NULL) {
		ret = -ENOMEM;
		goto err_readbuf;
	}

	printk("Fill buffer with random data\n");
	get_random_bytes(writebuf, WIN_SIZE);

	printk("Fill first window\n");
	copied = vme_master_write(master_res[0], writebuf,
			WIN_SIZE - (8 * sizeof(u32)), 0);
	if (copied != (WIN_SIZE - (8 * sizeof(u32)))) {
		ret = -EINVAL;
		goto err_copy;
	}

	printk("Trigger copy\n");
	copied = vme_master_write(master_res[0], writebuf, 1,
			WIN_SIZE - (8 * sizeof(u32)));
	if (copied != 1) {
		ret = -EINVAL;
		goto err_copy;
	}

	return 0;

err_copy:
	kfree(readbuf);
err_readbuf:
	kfree(writebuf);
err_writebuf:
	vme_lm_detach(lm_res, 0);
err_lm_attach:
err_lm_set:
	vme_irq_free(dev, 1, 1);
err_int:
	i = 2;
err_slave_set:
	while (i > 0) {
		i--;
		vme_slave_set(slave_res[i], 0, 0x0, 0x0, 0, VME_A32, VME_SCT);
	}

	i = 2;
err_alloc:
	while (i > 0) {
		i--;
		vme_free_consistent(slave_res[i], WIN_SIZE, slave_ptr[i],
				slave_dma[i]);
	}

	i = 2;
err_master_set:
	while (i > 0) {
		i--;
		vme_master_set(master_res[i], 0, 0x0, 0x0, 0, VME_A32, VME_SCT);
	}

	vme_lm_free(lm_res);
err_lm:
	i = 2;
err_slave:
	while (i > 0) {
		i--;
		vme_slave_free(slave_res[i]);
	}

	i = 2;
err_master:
	while (i > 0) {
		i--;
		vme_master_free(master_res[i]);
	}

	return ret;

}

struct vme_driver vme_test_mod_dvr = {
	.name = "VME Test Module",
	.match = vme_test_mod_match,
	.probe = vme_test_mod_probe,
};

static int vme_test_mod_init(void)
{
	int ret;

	printk("Loading VME test module\n");

	ret = vme_register_driver(&vme_test_mod_dvr, 1);
	if (ret != 0) {
		printk("Registration failed\n");
		return 1;
	}

	return 0;
}
module_init(vme_test_mod_init);

static void vme_test_mod_exit(void)
{
#if 0
	int i;
#endif

	printk("Removing VME test module\n");
#if 0
	vme_lm_detach(lm_res, 0);

	vme_irq_free(vme_dev, 1, 1);

	i = 2;
	while (i > 0) {
		i--;
		vme_slave_set(slave_res[i], 0, 0x0, 0x0, 0, 0, 0);
	}

	i = 2;
	while (i > 0) {
		i--;
		vme_free_consistent(slave_res[i], WIN_SIZE, slave_ptr[i],
				slave_dma[i]);
	}

	i = 2;
	while (i > 0) {
		i--;
		vme_master_set(master_res[i], 0, 0x0, 0x0, 0, 0, 0);
	}

	vme_lm_free(lm_res);

	i = 2;
	while (i > 0) {
		i--;
		vme_slave_free(slave_res[i]);
	}

	i = 2;
	while (i > 0) {
		i--;
		vme_master_free(master_res[i]);
	}
#endif
	vme_unregister_driver(&vme_test_mod_dvr);
}
module_exit(vme_test_mod_exit);

MODULE_LICENSE("GPL");
